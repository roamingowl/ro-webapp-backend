module.exports = {
    DIRS: {
        CONFIG: 'config'
    },
    ENVS: {
        PROD: 'production',
        DEV: 'development'
    }
}