'use strict';
const winston = require('winston');
const config = require('./../common/config');

let logger = module.exports = new winston.Logger({
    level: config.logs.file.level,
    transports: [
        new (winston.transports.File)(config.logs.file)
    ]
});