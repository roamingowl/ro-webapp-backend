'use strict';
//TODO: extract to own repo ?
const path = require('path');
const debug = require('debug')('config');
const consts = require('./../constants');

let configFileName = path.join('./../../', consts.DIRS.CONFIG, (process.env.NODE_ENV ? (process.env.NODE_ENV === consts.ENVS.PROD ? '' : process.env.NODE_ENV) : consts.ENVS.DEV), '/config.js');
debug('Got env ', process.env.NODE_ENV, ', config file resolved: ', configFileName);

module.exports = require(configFileName);