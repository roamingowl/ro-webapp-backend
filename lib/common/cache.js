const redis = require('./../cache/redis');
const debug = require('debug')('cache');

class cache {
    static async get(key) {
        debug(`reading by key ${key}`);
        try {
            let value = await redis.getAsync(key)
            debug(`got value for key ${key}: ${value.length} bytes`);
            return value;

        } catch (e) {
            debug(`value for key ${key} NOT FOUND!`);
            return null;
        }
    }

    static async set(key, value, ttl = 900) {
        debug(`setting value for key ${key}`);
        return await redis.setAsync(key, value, 'EX', ttl);
    }
}

module.exports = cache;