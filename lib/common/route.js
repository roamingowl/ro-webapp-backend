'use strict';
module.exports = class Route {
    constructor(options) {
        this.options = options || {};
        this.context = null;
    }

    setContext(context) {
        this.context = context;
    }

    main(req, res) {
        throw new Error('not implemented!')
    }

    getAllSteps() {
        return [
            this.main
        ]
    }
}