'use strict';
const debug = require('debug')('ro-webapp:getLanguageRoute');
const packageJson = require('./../../../package.json');
const BaseRoute = require('./../../common/route');
const router = require('./../../router');
const fs = require('fs');
const process = require('process');
const path = require('path');

class Route extends BaseRoute {
    localeExists(locale) {
        return new Promise((resolve) => {
            debug ('Lang file ', path.join(process.cwd(), 'i18n', locale + '.js'));
            fs.stat(path.join(process.cwd(), 'i18n', locale + '.js'), (err, stats) => {
                if (err) {
                    console.error('Language load error ', err);
                    return resolve(false);
                }
                return resolve(true);
            })
        })
    }

    /**
     * @override
     */
    getConfig() {
        return {
            method: 'get',
            path: '/api/v1.0/app/languages/:locale',
            name: 'getLanguage'
        }
    }

    main(req, res) {
        debug('Route', this.options.name);
        let locale = req.params.locale;

        return this.localeExists(locale)
        .then((exists) => {
            debug('Language file exists: ', exists);
            if (!exists) {
                throw new Error('language does not exists!');
            }
            debug('Loading file');
            let dictionary = require(path.join(process.cwd(), 'i18n', locale + '.js'));
            res.status(200).json({meta: {version: packageJson.version}, dictionary: dictionary});
        })
    }
}

router.loadRoute(Route);