'use strict';
const debug = require('debug')('getArticle');

const configuration = require('./../../../common/config');
const knex = require('./../../../database/local').knex;
const BaseRoute = require('./../../../common/route');
const sequelize = require('./../../../database/local').sequelize;
const router = require('./../../../router');
const NotFoundError = require('./../../../error/notFound');
const Articles = require('./../../../models/app/articles');
const Cache = require('./../../../common/cache');

//builder
/*
module.exports = builder
.build({
    method: 'get',
})
.addStep((req, res) => {
    ...
})
 */

class Route extends BaseRoute {
    /**
     * @override
     */
    getConfig() {
        return {
            method: 'get',
            path: '/api/v1.0/articles/:articleid?',
            name: 'getArticle'
        }
    }

    async main(req, res) {
        debug(`Getting article`, req.params.articleid);
        let articleId = parseInt(req.params.articleid);
        if (!articleId && req.params.articleid !== 'index') {
            let response = await Cache.get(req.originalUrl);
            if (response) {
                res.header('from-redis-cache', true);
                return res.status(200).json(JSON.parse(response));
            }
            let limit = req.query && parseInt(req.query.rowsPerPage);
            if (!limit) {
                limit = 10;
            }
            let offset = req.query && req.query.page && (limit * (parseInt(req.query.page) - 1));
            if (!offset) {
                offset = 0;
            }
            let count = await Articles.getAllCount();
            let articles = [];
            if (count) {
                let articleIds = await Articles.getAll(limit, offset);
                await Promise.all(articleIds.map(async (article) => {
                    articles.push(await Articles.getOne(article.id));
                }))
            }
            response = {
                rows: articles,
                count: count,
                pages: Math.ceil(count / limit)
            };
            res.header('from-redis-cache', false);
            await Cache.set(req.originalUrl, JSON.stringify(response));
            return res.status(200).json(response);
        } else if (req.params.articleid === 'index') {
            let response = await Cache.get(req.originalUrl);
            if (response) {
                res.header('from-redis-cache', true);
                return res.status(200).json(JSON.parse(response));
            }
            let articles = {};
            let articleIds = await Articles.getAll(null, null, ['id', 'date_changed']);
            articleIds.map((article) => {
                articles[article.id] = article.date_changed;
            });
            res.header('from-redis-cache', false);
            await Cache.set(req.originalUrl, JSON.stringify(articles));
            return res.status(200).json(articles);
        }

        let article;
        article = await Cache.get('article_' + articleId);
        if (article) {
            res.header('from-redis-cache', true);
            return res.status(200).json(JSON.parse(article));
        }
        article = await Articles.getOne(articleId);
        await Cache.set('article_' + articleId, JSON.stringify(article));
        res.header('from-redis-cache', false);
        return res.status(200).json(article);
    }
}

router.loadRoute(Route);