'use strict';
const debug = require('debug')('ro-webapp:pingRoute');

const configuration = require('./../../common/config');
const BaseRoute = require('./../../common/route');
const router = require('./../../router');

class Route extends BaseRoute {
    /**
     * @override
     */
    getConfig() {
        return {
            method: 'get',
            path: '/api/v1.0/ping',
            name: 'getPing'
        }
    }

    main(req, res) {
        debug('Route', this.options.name)
        res.status(200).json('pong');
    }
}

router.loadRoute(Route);