'use strict';
const debug = require('debug')('getPlant');

const configuration = require('./../../../common/config');
const knex = require('./../../../database/local').knex;
const BaseRoute = require('./../../../common/route');
const sequelize = require('./../../../database/local').sequelize;
const router = require('./../../../router');
const NotFoundError = require('./../../../error/notFound');
const Plants = require('./../../../models/app/plants');
const Cache = require('./../../../common/cache');

//builder
/*
module.exports = builder
.build({
    method: 'get',
})
.addStep((req, res) => {
    ...
})
 */

class Route extends BaseRoute {
    /**
     * @override
     */
    getConfig() {
        return {
            method: 'get',
            path: '/api/v1.0/plants/:plantid?',
            name: 'getPlant'
        }
    }

    async main(req, res) {
        debug(`Getting plant`, req.params.plantid);
        let plantId = parseInt(req.params.plantid);
        if (!plantId && req.params.plantid !== 'index') {
            let response = await Cache.get(req.originalUrl);
            if (response) {
                res.header('from-redis-cache', true);
                return res.status(200).json(JSON.parse(response));
            }
            let limit = req.query && parseInt(req.query.rowsPerPage);
            if (!limit) {
                limit = 10;
            }
            let offset = req.query && req.query.page && (limit * (parseInt(req.query.page) - 1));
            if (!offset) {
                offset = 0;
            }
            let count = await Plants.getAllCount();
            let plants = [];
            if (count) {
                let plantIds = await Plants.getAll(limit, offset);
                await Promise.all(plantIds.map(async (plant) => {
                    plants.push(await Plants.getOne(plant.id));
                }))
            }
            response = {
                rows: plants,
                count: count,
                pages: Math.ceil(count / limit)
            };
            res.header('from-redis-cache', false);
            await Cache.set(req.originalUrl, JSON.stringify(response));
            return res.status(200).json(response);
        } else if (req.params.plantid === 'index') {
            let response = await Cache.get(req.originalUrl);
            if (response) {
                res.header('from-redis-cache', true);
                return res.status(200).json(JSON.parse(response));
            }
            let plants = {};
            let plantIds = await Plants.getAll(null, null, ['id', 'modified_date']);
            plantIds.map((plant) => {
                plants[plant.id] = plant.modified_date;
            });
            res.header('from-redis-cache', false);
            await Cache.set(req.originalUrl, JSON.stringify(plants));
            return res.status(200).json(plants);
        }

        let plant;
        plant = await Cache.get('plant_' + plantId);
        if (plant) {
            res.header('from-redis-cache', true);
            return res.status(200).json(JSON.parse(plant));
        }
        plant = await Plants.getOne(plantId);
        await Cache.set('plant_' + plantId, JSON.stringify(plant));
        res.header('from-redis-cache', false);
        return res.status(200).json(plant);
    }
}

router.loadRoute(Route);