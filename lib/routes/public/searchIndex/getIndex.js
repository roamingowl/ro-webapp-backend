'use strict';
const debug = require('debug')('getSearchIndexIndex');

const configuration = require('./../../../common/config');
const knex = require('./../../../database/local').knex;
const BaseRoute = require('./../../../common/route');
const sequelize = require('./../../../database/local').sequelize;
const router = require('./../../../router');
const NotFoundError = require('./../../../error/notFound');

//builder
/*
module.exports = builder
.build({
    method: 'get',
})
.addStep((req, res) => {
    ...
})
 */

class Route extends BaseRoute {
    /**
     * @override
     */
    getConfig() {
        return {
            method: 'get',
            path: '/api/v1.0/search-index/index',
            name: 'getSearchIndexIndex'
        }
    }

    main(req, res) {
        let searchIn = req.query.searchIn;
        let itemId = req.params.itemid;
        debug('Route', req.params, req.query);
        let query;
        if (searchIn === 'plants') {
            query = knex.select('plants.id', 'plants.modified_date AS updated_at')
            .from('plants') //TODO use app model idiot!
            .where({
                'plants.is_active': 1,
                'plants.is_deleted': 0
            });
        } else if (searchIn === 'expositions') {
            query = knex.select('article.id', 'article.date_changed AS updated_at')
            .from('article') //TODO use app model idiot!
            .where({
                //'article.published': 1, //tODO: fix this
                'article.deleted_at': null
            });
        }
        debug('Executing query ', query.toString())
        return sequelize.query(query.toString(), {type: sequelize.QueryTypes.SELECT})
        .then((rows) => {
            if (rows.length === 0) {
                throw new NotFoundError('No items found');
            }
            let response = {};
            rows.map((item) => {
                response[item.id] = item.updated_at;
            });
            return res.status(200).json(response);
        })
        .catch((e) => {
            return res.status(404).json({
                errorCode: 'not_found',
                message: 'No items found',
                stack: e.stack,
                code: e.code
            });
        })
    }
}

router.loadRoute(Route);