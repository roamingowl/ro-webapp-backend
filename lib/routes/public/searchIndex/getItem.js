'use strict';
const debug = require('debug')('getSearchIndexItem');

const configuration = require('./../../../common/config');
const knex = require('./../../../database/local').knex;
const BaseRoute = require('./../../../common/route');
const sequelize = require('./../../../database/local').sequelize;
const router = require('./../../../router');
const NotFoundError = require('./../../../error/notFound');

//builder
/*
module.exports = builder
.build({
    method: 'get',
})
.addStep((req, res) => {
    ...
})
 */

class Route extends BaseRoute {
    /**
     * @override
     */
    getConfig() {
        return {
            method: 'get',
            path: '/api/v1.0/search-index/:itemId',
            name: 'getSearchIndexItem'
        }
    }

    main(req, res) {
        let searchIn = req.query.searchIn;
        let itemId = req.params.itemid;
        debug('Route', req.params, req.query);

        let query;
        //TODO: find a better way than ifs!
        if (searchIn === 'plants') {
            query = knex.select('plants.id', 'plants.name', 'plants.name_en', 'plants.name_lat', 'plants.name_de', 'plants.name_sk', 'plants.category_id', 'plants.family_id', 'plants.modified_date AS updated_at', 'plants.content_length', 'plants.audio_cz')
            .from('plants') //TODO use app model idiot!
            .where({
                'plants.is_active': 1,
                'plants.is_deleted': 0,
                'plants.id': itemId
            });
        } else if (searchIn === 'expositions') {
            query = knex.select('article.id', 'article.title', 'article.title_en', 'article.author', 'article.date_changed AS updated_at')
            .from('article') //TODO use app model idiot!
            .where({
                //'plants.published': 1, //TODO: make this work
                'article.deleted_at': null,
                'article.id': itemId
            });
        }
        debug('Executing query ', query.toString())
        return sequelize.query(query.toString(), {type: sequelize.QueryTypes.SELECT})
        .then((rows) => {
            if (rows.length === 0) {
                throw new NotFoundError('Item not found');
            }
            return res.status(200).json(rows[0]);
        })
        .catch((e) => {
            return res.status(404).json({
                errorCode: 'not_found',
                message: 'Item not found',
                stack: e.stack,
                code: e.code
            });
        })
    }
}

router.loadRoute(Route);