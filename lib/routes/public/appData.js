'use strict';
const debug = require('debug')('ro-webapp:getAppDataRoute');
const configuration = require('./../../common/config');
const knex = require('./../../database/local').knex;
const BaseRoute = require('./../../common/route');
const sequelize = require('./../../database/local').sequelize;
const router = require('./../../router');
const Settings = require('./../../models/app/settings');

//builder
/*
module.exports = builder
.build({
    method: 'get',
})
.addStep((req, res) => {
    ...
})
 */

class Route extends BaseRoute {
    /**
     * @override
     */
    getConfig() {
        return {
            method: 'get',
            path: '/api/v1.0/app/settings',
            name: 'getAppData'
        }
    }

    main(req, res) {
        return Settings.getAllAsMap()
        .then((settings) => {
            return res.status(200).json(settings);
        })
    }
}

router.loadRoute(Route);