'use strict';
const debug = require('debug')('getMarkers');

const configuration = require('./../../../common/config');
const knex = require('./../../../database/local').knex;
const BaseRoute = require('./../../../common/route');
const sequelize = require('./../../../database/local').sequelize;
const router = require('./../../../router');
const NotFoundError = require('./../../../error/notFound');
const lodash = require('lodash');
const Markers = require('./../../../models/app/markers');
const cache = require('./../../../common/cache');
const md5 = require('md5');

//builder
/*
module.exports = builder
.build({
    method: 'get',
})
.addStep((req, res) => {
    ...
})
 */

class Route extends BaseRoute {
    /**
     * @override
     */
    getConfig() {
        return {
            method: 'get',
            path: '/api/v1.0/markers/',
            name: 'getMarkers'
        }
    }

    //exhibitions
    //SELECT * FROM map_marker inner join article on article.map_marker_id=map_marker.id and map_marker.map_overlay_id=1 and article.deleted_at is null
    //all_markers
    /*
    SELECT * FROM map_marker
    left join plants as plma on plma.map_marker_id=map_marker.id and plma.is_deleted=0 and plma.is_active=1
    left join map_overlay on map_overlay.id=map_marker.map_overlay_id and map_overlay.id<>1
    left join plants as plmo on plmo.map_overlay_id=map_overlay.id and plmo.is_deleted=0 and plmo.is_active=1
    where
    (plma.is_active=1 or plmo.is_active=1) and marker_visible=1
    group by map_marker.id
    */
    //blooming
    /*SELECT map_marker.*,tags.* FROM map_marker
    left join plants as plma on plma.map_marker_id=map_marker.id and plma.is_deleted=0 and plma.is_active=1
    left join map_overlay on map_overlay.id=map_marker.map_overlay_id and map_overlay.id<>1
    left join plants as plmo on plmo.map_overlay_id=map_overlay.id and plmo.is_deleted=0 and plmo.is_active=1
    left join plant_tags on (plmo.id=plant_tags.plant_id or plma.id=plant_tags.plant_id)
    left join tags on tags.id=plant_tags.tag_id
    where
    (plma.is_active=1 or plmo.is_active=1) and marker_visible=1
    and tags.name='Právě kvete'
    //
    //bearing friuts
    /*SELECT map_marker.*,tags.* FROM map_marker
    left join plants as plma on plma.map_marker_id=map_marker.id and plma.is_deleted=0 and plma.is_active=1
    left join map_overlay on map_overlay.id=map_marker.map_overlay_id and map_overlay.id<>1
    left join plants as plmo on plmo.map_overlay_id=map_overlay.id and plmo.is_deleted=0 and plmo.is_active=1
    left join plant_tags on (plmo.id=plant_tags.plant_id or plma.id=plant_tags.plant_id)
    left join tags on tags.id=plant_tags.tag_id
    where
    (plma.is_active=1 or plmo.is_active=1) and marker_visible=1
    and tags.name='Právě plodí'
    group by map_marker.id*/
    //
    /*
    plants: {
            $title: 'Plants',
            type: 'plant',
            $subFilters: { //TODO: maybe there is a better way to define multi level filters?
                '$all': {
                    $title: 'All plants',
                },
                blooming: {
                    $title: 'Blooming',
                    tags: {
                        $contains: 'blooming'
                    }
                },
                bearingFruits: {
                    $title: 'Bearing fruits',
                    tags: {
                        $contains: 'bearingFruits'
                    }
                },
            }
        },
     */

    async main(req, res) {
        debug('Route', req.params, req.query);
        let filter = {};
        let cacheHash = 'getMarkers';
        let result = await cache.get(cacheHash);
        if (result) {
            let textResult = result.toString();
            res.header('Content-Type', 'application/json');
            res.header('Content-Length', textResult.length);
            res.header('from-redis-cache', true);
            res.send(textResult);
            return;
        }
        let plantMarkers = await Markers.getAllByType('plants');
        let expositionsMarkers = await Markers.getAllByType('expositions');
        let allMarkers = plantMarkers.concat(expositionsMarkers);
        await cache.set(cacheHash, JSON.stringify(allMarkers));
        res.header('from-redis-cache', false);
        res.json(allMarkers);


        // .then((markers) => {
            // return cache.set(cacheHash, JSON.stringify(markers))
            // .then(() => {
            //     return markers;
            // })
        // })
        // .then((markers) => {
        //     res.header('from-redis-cache', false);
        //     res.json(markers);
        // })
        // .catch((e) => {
        //     return res.status(400).json({
        //         errorCode: 'error',
        //         message: e.message,
        //         stack: e.stack,
        //         code: e.code
        //     });
        // })

        // let query;
        // if (queryFilter.type === 'plant') {
        //     //tableName = sequelize.models[????].definition.tableName;
        // } else if (queryFilter.type === 'expositions') {
        //     //SELECT * FROM map_marker inner join article on article.map_marker_id=map_marker.id and map_marker.map_overlay_id=1 and article.deleted_at is null
        //
        //      query = knex.select('map_marker.*')
        //     .from('map_marker') //TODO use app model idiot!
        //     .innerJoin('article', 'article.map_marker_id', 'map_marker.id')
        //     .where({
        //         'article.deleted_at': null,
        //         'map_marker.map_overlay_id': 1,
        //         'map_marker.marker_visible': 1
        //     });
        // } else {
        //     return res.status(400).json({
        //         errorCode: 'unknown_type',
        //         message: `Type ${queryFilter.type} is not known`,
        //         // stack: e.stack,
        //         // code: e.code
        //     });
        // }
        // return sequelize.query(query.toString(), {type: sequelize.QueryTypes.SELECT})
        // .then((result) => {
        //     res.json(result);
        // })

        /////
    }
}

router.loadRoute(Route);