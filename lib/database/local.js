'use strict'
const Sequelize = require('sequelize');
const path = require('path');
const glob = require('glob');
const debug = require('debug')('ro-webapp:localDatabase');

const configuration = require('./../common/config');
const SequelizeBuilder = require('./../models/sequelizeBuilder');

const knex = require('knex')({
    client: 'mysql',
    debug: configuration.storage.mysql.debug,
});

let sequelize = new Sequelize(configuration.storage.mysql.database, configuration.storage.mysql.user, configuration.storage.mysql.password, {
    host: configuration.storage.mysql.host,
    dialect: 'mysql',
    pool: configuration.storage.mysql.pool
});

let files = glob.sync(process.cwd() + '/lib/models/sequelize/**/*.js', {});
for (let file of files) {
    let modelDefinition = require(file);
    SequelizeBuilder.build(sequelize, modelDefinition.definition);
    debug(modelDefinition.name + ' DB model registered (sequelize)');
}

module.exports.sequelize = sequelize;
module.exports.knex = knex;
