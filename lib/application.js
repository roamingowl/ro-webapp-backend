'use strict';
const express = require('express');
const EventEmitter = require('events');
// const session = require('express-session');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');
const debug = require('debug')(`ro-webapp:${path.basename(__filename)}`);
const csrf = require('csurf');
const passport = require('passport');
const process = require('process');

const configuration = require('./common/config');

const errorLog = require('./util/errorLog');

class Application extends EventEmitter {
    constructor() {
        super();
        this.expressApp = new express();
    }

    build() {
        return new Promise((resolve) => {
            debug('Building app');
            //setup express
            this.expressApp.use(cors());
            this.expressApp.use(cookieParser());
            let sessionConfiguration = configuration.session;
            if (this.expressApp.get('env') === 'production') {
                this.expressApp.set('trust proxy', 1); // trust first proxy
                sessionConfiguration.cookie.secure = true // serve secure cookies
            }

            this.expressApp.use(bodyParser.urlencoded({extended: true}))
            this.expressApp.use(bodyParser.json())

            this.expressApp.use('/css', express.static('public/css'));
            this.expressApp.use('/js', express.static('public/js'));
            this.expressApp.use('/admin/font', express.static('public/admin/font'));
            this.expressApp.use('/login/font', express.static('public/login/font'));
            this.expressApp.use('/img', express.static('public/img'));
            this.expressApp.use('/files/audio', express.static('data/audio'));
            //setup end

            require('./router');
            debug('Build complete!');
            return resolve();
        })
    }

    listen() {
        let port = process.env.LISTEN_PORT || configuration.listen.port;
        this.expressApp.listen(port, (err) => {
            if (err) {
                console.error('Unable to listen on port ', port, err);
                this.emit('error', err);
                return;
            }
            debug('Listening on port ', port);
            //errorLog.log('error', 'Application started');
            this.emit('listen', port);
        })
    }
}
module.exports = new Application();