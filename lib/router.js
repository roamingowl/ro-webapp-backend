'use strict';
const Promise = require('bluebird');
const debug = require('debug')('router');

const BaseRoute = require('./common/route');
const localDb = require('./database/local');
const glob = require('glob');
const process = require('process');
const mainApp = require('./application');

/**
 * @class Router
 *
 */
class Router {
    constructor() {
        this.expressApp = mainApp.expressApp;
        this.routes = [];
    }

    getRouteInstance(routeDefinition, options) {
        let routeInstance = new routeDefinition(options);
        return routeInstance;
    }

    buildRoutes() {
        //proxies
        let files = glob.sync(process.cwd() + '/lib/routes/**/*.js', {});
        for (let file of files) {
            require(file);
        }
        debug('Routes list:');
        this.printRoutes();
    }

    processRoute(routeInstance, req, res) {
        let stepIndex = 0;
        let routeSteps = routeInstance.getAllSteps();
        debug('route "' + routeInstance.getConfig().name + '" has ' + routeSteps.length + ' steps');

        function loop() {
            if (stepIndex >= routeSteps.length - 1) {
                return;
            }
            stepIndex++;
            return Promise.resolve(routeSteps[stepIndex].call(routeInstance, req, res)).then(loop);
        }

        let result = routeSteps[stepIndex].call(routeInstance, req, res);
        if (result === undefined || result.then === undefined) {
            result = Promise.resolve(result);
        }
        return result.then(loop);
    }

    loadRoute(routeDefinition, options) {
        let instance = new routeDefinition(options);
        this.addRoute(instance.getConfig().method, instance.getConfig().path, instance, {name: instance.getConfig().name});
    }

    /**
     * @method addRoute
     * @param type
     * @param url
     * @param routeInstance
     * @param options
     */
    addRoute(type, url, routeInstance, options) {
        debug('Adding route ', type, url);
        if (!(routeInstance instanceof BaseRoute)) {
            throw new Error('Route must be instance of BaseRoute!');
        }
        this.routes.push({type: type, url: url.toLowerCase(), instance: routeInstance, options: options});
        this.expressApp[type.toLowerCase()](url.toLowerCase(), (req, res, next) => {
            debug('route ', type, url);
            let routeContext = {};
            routeContext.name = options.name || '';
            routeInstance.setContext(routeContext);
            this.processRoute(routeInstance, req, res)
            .then(function (result) {
                //TODO: handle result - may be redirect or something...
                debug('route finished');
                if (!res.statusCode) {
                    throw new Error('no response');
                }
                return null;
            })
            .catch(function (err) { //TODO: extract to error handler
                debug('Route error', err.code);
                console.error('Route error', err);
                res.status(400);
                res.json({
                    errorCode: err.statusCode,
                    message: err.message,
                    stack: err.stack,
                    code: err.code
                });
            })
        });
    }

    printRoutes() {
        this.routes.forEach((route) => {
            let name = (route.options && route.options.name) ? '@' + route.options.name : '';
            debug(`${route.type.toUpperCase()}: ${route.url} ${name}`);
        });
    }
}

const router = new Router(mainApp.expressApp);
module.exports = router;
router.buildRoutes();
