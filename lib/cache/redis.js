const bluebird = require('bluebird');
const redis = require("redis");
const config = require('./../common/config');

let client = redis.createClient(config.storage.redis);

//TODO genralize this to cache storage interface
module.exports = bluebird.promisifyAll(client);
