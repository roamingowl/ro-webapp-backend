const configuration = require('./../../common/config');
const DataTypes = require('sequelize').DataTypes;
const SettingsNames = require('./../app/settingsNames');

const NAME = 'Settings';

module.exports.name = NAME;
module.exports.definition = {
    name: NAME,
    fields: {
        name: {type: DataTypes.STRING(255), primaryKey: true},
        value: {type: DataTypes.TEXT}
    },
    tableName: configuration.storage.mysql.tablePrefix + 'webapp_settings',
    timestamps: false,
    paranoid: false,
};
module.exports.settingNames = SettingsNames;