const DataTypes = require('sequelize').DataTypes;

const NAME = 'ImageCompat';

module.exports.name = NAME;
module.exports.definition = {
    name: NAME,
    fields: {
        id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
        name: {type: DataTypes.STRING(256)},
        width: {type: DataTypes.INTEGER},
        height: {type: DataTypes.INTEGER},
        type: {type: DataTypes.STRING(20)},
        size: {type: DataTypes.INTEGER},
        url: {type: DataTypes.STRING(512)},
        preview_url: {type: DataTypes.STRING(255)},
        path: {type: DataTypes.STRING(512)},
        description: {type: DataTypes.TEXT},
        description_en: {type: DataTypes.TEXT},
        authors: {type: DataTypes.TEXT},
        authors_en: {type: DataTypes.TEXT},
        display: {type: DataTypes.STRING(45)},
        display_vertical_offset: {type: DataTypes.INTEGER},
        original_name: {type: DataTypes.STRING(256)},
    },
    tableName: 'image',
    timestamps: true,
    paranoid: true,
}