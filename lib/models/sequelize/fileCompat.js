const DataTypes = require('sequelize').DataTypes;

const NAME = 'FileCompat';

module.exports.name = NAME;
module.exports.definition = {
    name: NAME,
    fields: {
        id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
        name: {type: DataTypes.STRING(255)},
        meta: {type: DataTypes.TEXT},
        type: {type: DataTypes.STRING(45)},
        size: {type: DataTypes.INTEGER},
        path: {type: DataTypes.TEXT},
        url: {type: DataTypes.TEXT},
        original_name: {type: DataTypes.STRING(255)},
    },
    tableName: 'files',
    timestamps: true,
    paranoid: true,
};