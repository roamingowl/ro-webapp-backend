"use strict";
const sequelize = require('./../../database/local').sequelize;
const SettingsDefinition = require('./../sequelize/settings');

module.exports = class {
    static async getAllAsMap() {
        console.log(sequelize.models[SettingsDefinition.definition.name].tableName
        );
        return sequelize.models[SettingsDefinition.definition.name].findAll()
        .then((rows) => {
            let associativeSettings = {};
            rows.map((item) => {
                associativeSettings[item.name] = item.value;
            })
            return associativeSettings;
        })
    }
}