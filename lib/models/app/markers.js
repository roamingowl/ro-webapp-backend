"use strict";
const sequelize = require('./../../database/local').sequelize;
const knex = require('./../../database/local').knex;

/**
 * @class Markers
 * @type {module.Markers}
 */
module.exports = class Markers {
    static getAllPlantsQuery() {
        return knex.column(knex.raw('\'plant\' as type'), 'map_marker.id','map_marker.updated_at', knex.raw('ifnull(plma.name, plmo.name) as plant_name'), knex.raw('ifnull(plma.tags, plmo.tags) as tags'),
            knex.raw('ifnull(plma.name_en, plmo.name_en) as plant_name_en'),
            knex.raw('ifnull(plma.name_lat, plmo.name_lat) as plant_name_lat'), 'map_marker.latitude', 'map_marker.longitude', knex.raw('ifnull(plma.id, plmo.id) as plant_id'))
        .from('map_marker') //TODO use app model idiot!
        .leftJoin('plants as plma', knex.raw('plma.map_marker_id=map_marker.id and plma.is_deleted=? and plma.is_active = ?', [0, 1]))//'plma.map_marker_id', 'map_marker.id').andOn('plma.is_deleted', '=', '0').andOn('plma.is_active', '=', '1')
        .leftJoin('map_overlay', knex.raw('map_overlay.id=map_marker.map_overlay_id and map_overlay.id <> ?', [1]))//.andOn('map_overlay.id', '<>', '1')
        .leftJoin('plants as plmo', knex.raw('plmo.map_overlay_id=map_overlay.id and plmo.is_deleted = ? and plmo.is_active = ?', [0, 1]))//.andOn('plmo.is_deleted', '=', '0').andOn('plmo.is_active', '=', '1')
        .where({
            'map_marker.marker_visible': 1
        })
        .andWhereRaw('(plma.is_active=? or plmo.is_active=?)', [1, 1])
        .groupBy('map_marker.id')
    }

    static getAllExpositionsQuery() {
        return knex.column(knex.raw('\'exposition\' as type'), 'map_marker.id', 'map_marker.updated_at', 'article.title', 'article.title_en', 'map_marker.latitude', 'map_marker.longitude', knex.raw('article.id as article_id'))
        .from('map_marker') //TODO use app model idiot!
        .innerJoin('article', 'article.map_marker_id', 'map_marker.id')
        .where({
            'article.deleted_at': null,
            'map_marker.map_overlay_id': 1,
            'map_marker.marker_visible': 1
        });
    }

    /**
     * @memberOf Markers
     * @param type
     * @return {Promise.<*>}
     */
    static async getAllByType(type) {
        let query;
        if (type === 'expositions') {
            query = this.getAllExpositionsQuery();
        } else if (type === 'plants') {
            query = this.getAllPlantsQuery();
        } else {
            throw new Error('Unknown type');
        }
        return sequelize.query(query.toString(), {type: sequelize.QueryTypes.SELECT})
        .then((rows) => {
            if (type === 'expositions') {
                return rows;
            }
            if (type === 'plants') {
                rows.map((item) => {
                    try {
                        item.tags = JSON.parse(item.tags);
                    } catch (e) {
                        //ignore
                    }
                    return item;
                })
                return rows;
            }
        })
    }
}