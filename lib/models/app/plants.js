"use strict";
const sequelize = require('./../../database/local').sequelize;
const knex = require('./../../database/local').knex;

/**
 * @class Markers
 * @type {module.Markers}
 */
module.exports = class Plants {
    static $sanitize(item) {
        if (item.tags) {
            item.tags = JSON.parse(item.tags);
        }
        return item;
    }

    static async getAllCount() {
        let query = knex
        .count('plants.id AS cnt')
        .from('plants')
        .where('is_active', 1)
        .andWhere('is_deleted', 0);
        let result = await sequelize.query(query.toString(), {type: sequelize.QueryTypes.SELECT});
        return result[0]['cnt'];
    }

    static async getAll(limit, offset, columns = ['id']) {
        let query = knex
        .select(columns.map((colName)=>{return 'plants.' + colName}))
        .from('plants')
        .where('is_active', 1)
        .andWhere('is_deleted', 0);
        if (limit) {
            query.limit(limit);
        }
        if (offset) {
            query.offset(offset);
        }
        let result = await sequelize.query(query.toString(), {type: sequelize.QueryTypes.SELECT});
        return result;
    }

    /**
     * @return String
     * @param {Array} blocks
     */
    static processBlocks(blocks, plant) {
        let output = {
            en: '',
            cs: '',
            enPlain: '',
            csPlain: ''
        };
        blocks.map((block) => {
            if (block.type === 0) { //text
                output.cs += `<TextBlock block-id="${block.id}" ${block.background_color ? 'backgroud-color="' + block.background_color + '"' : ''}>${block.content}</TextBlock>`;
                output.en += `<TextBlock block-id="${block.id}" ${block.background_color ? 'backgroud-color="' + block.background_color + '"' : ''}>${block.content_en || block.content}</TextBlock>`;
                output.csPlain += block.content_plain;
                output.enPlain += block.content_en_plain || block.content_plain;
            } else if (block.type === 1) {
                let blockImagesCs = '';
                let blockImagesEn = '';
                block.images && block.images.map((image) => {
                    blockImagesCs += `<GalleryImage image-id="${image.id}" url="${image.url}" display-vertical-offset="${image.display_vertical_offset}" display="${image.display}" width="${image.width}" height="${image.height}" description="${image.description}"></GalleryImage>`
                    blockImagesEn += `<GalleryImage image-id="${image.id}" url="${image.url}" display-vertical-offset="${image.display_vertical_offset}" display="${image.display}" width="${image.width}" height="${image.height}" description="${image.description_en}"></GalleryImage>`
                });
                let blockTemplateCs = `<GalleryBlock block-id="${block.id}" ${block.display_vertical_offset ? 'display-vertical-offset="' + block.display_vertical_offset + '"' : ''} ${block.display ? 'display="' + block.display + '"' : ''} ${block.background_color ? 'backgroud-color="' + block.background_color + '"' : ''}>${blockImagesCs}</GalleryBlock>`;
                let blockTemplateEn = `<GalleryBlock block-id="${block.id}" ${block.display_vertical_offset ? 'display-vertical-offset="' + block.display_vertical_offset + '"' : ''} ${block.display ? 'display="' + block.display + '"' : ''} ${block.background_color ? 'backgroud-color="' + block.background_color + '"' : ''}>${blockImagesEn}</GalleryBlock>`;
                output.cs += blockTemplateCs;
                output.en += blockTemplateEn;
            } else if (block.type === 2) {
                let simplifiuedPlant = {};
                Object.keys(plant).filter((keyName) => {
                    return ['name', 'name_en', 'name_lat', 'name_de', 'name_sk', 'occurrence', 'family_name', 'family_name_lat'].indexOf(keyName) > -1
                }).map((keyName) => {
                    simplifiuedPlant[keyName] = plant[keyName];
                })
                let blockTemplate = '<InfoBlock plant=\''+JSON.stringify(simplifiuedPlant)+'\'></InfoBlock>';
                output.cs += blockTemplate;
                output.en += blockTemplate;
            }
        });
        return output;
    }

    static async getOne(id) {
        let plant;
        await sequelize.transaction(async function (t) {
            let sql = "SELECT pl.*,f.name as family_name, f.name_lat as family_name_lat FROM plants pl LEFT JOIN plant_family f on f.id=pl.family_id  WHERE pl.id=? AND pl.is_active=1 AND is_deleted=0";
            let plants = await sequelize.query(sql, {
                replacements: [id],
                transaction: t,
                type: sequelize.QueryTypes.SELECT
            });
            if (!plants || plants.length === 0) {
                throw new NotFoundError('No plant found');
            }
            plant = plants[0];
            if (plant.audio_cz) {
                let sql = "SELECT * FROM files WHERE id=?";
                let result = await sequelize.query(sql, {
                    replacements: [plant.audio_cz],
                    transaction: t,
                    type: sequelize.QueryTypes.SELECT
                });
                if (result) {
                    plant.audio_cz_url = result[0].url;
                }
            }
            //imgs
            plant.images = [];
            sql = "SELECT i.*,pi.order FROM plant_image pi inner join image i ON i.id=pi.image_id WHERE pi.plant_id=? ORDER BY pi.`order` ASC";
            let images = await sequelize.query(sql, {
                replacements: [id],
                transaction: t,
                type: sequelize.QueryTypes.SELECT
            });
            if (images) {
                plant.images = images;
            }
            //authors
            sql = "SELECT a.* FROM plant_authors pa inner join authors a ON a.id=pa.author_id WHERE pa.plant_id=? AND a.is_deleted=0";
            let authors = await sequelize.query(sql, {
                replacements: [id],
                transaction: t,
                type: sequelize.QueryTypes.SELECT
            });
            if (authors) {
                plant.authors = authors;
            }
            //some tweaks
            plant.updated_at = plant.modified_date;
            //blocks
            sql = "SELECT pb.* FROM plant_block pb WHERE pb.plant_id=? AND deleted_at is null ORDER BY pb.`order` ASC";
            let blocks = await sequelize.query(sql, {
                replacements: [id],
                transaction: t,
                type: sequelize.QueryTypes.SELECT
            });
            if (blocks) {
                await Promise.all(blocks.map(async (block) => {
                    let sql = "SELECT i.* FROM plant_block_image pbi INNER JOIN image i on i.id=pbi.image_id WHERE pbi.plant_block_id=? ORDER BY pbi.`order` ASC";
                    let images = await sequelize.query(sql, {
                        replacements: [block.id],
                        transaction: t,
                        type: sequelize.QueryTypes.SELECT
                    });
                    block.images = images;
                    if (images && images.length > 0 && (!plant.images || plant.images.length === 0)) {
                        plant.images = images;
                    }
                    return block;
                }));
                //plant.blocks = blocks;
                plant.blocks = Plants.processBlocks(blocks, plant);
            }
        });
        return this.$sanitize(plant);
    }
}