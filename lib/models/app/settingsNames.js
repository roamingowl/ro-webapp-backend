module.exports = {
    BASE_URL: "baseUrl",
    DEFAULT_MAP_LATITUDE: "defaultMapLatitude",
    DEFAULT_MAP_LONGITUDE: "defaultMapLongitude",
    DEFAULT_MAP_ZOOM: "defaultMapZoom",
    MAX_MAP_ZOOM: "maxMapZoom",
    MAIN_LAYER_TILES_URL: "mainLayerTilesUrl", //TODO: add base map tiles url
    MAP_COPYRIGHT: "mapCopyright",
    IMAGES_URL: 'imagesUrl'
}