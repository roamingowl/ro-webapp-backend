module.exports = class SequelizeBuilder {
    static build(sequelize, definition) {
        return sequelize.define(definition.name, definition.fields, {
            tableName: definition.tableName,
            timestamps: definition.timestamps,
            paranoid: definition.paranoid,
            underscored: true
        });
    }
}