'use strict';
const app = require('./lib/application');
app.on('listen', (port) => {
    console.log('Listening on port ' + port)
});
app.on('error', (err) => {
    console.log('Application error', err, err.stack);
});
app.build()
.then(() => {
    app.listen();
})