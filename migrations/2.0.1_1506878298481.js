//UPDATE botgar.plants set modified_date=now() where modified_date is null;
const sequelize = require('./../lib/database/local').sequelize

module.exports.up = async function() {
    return sequelize.query('UPDATE plants set modified_date=now() where modified_date is null;', {type: sequelize.QueryTypes.UPDATE})
}

module.exports.down = function() {
    return;
}