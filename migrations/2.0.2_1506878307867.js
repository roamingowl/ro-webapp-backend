//UPDATE botgar.plants set modified_date=now() where modified_date is null;
const sequelize = require('./../lib/database/local').sequelize

module.exports.up = async function() {
    /*
    ALTER TABLE `botgar`.`map_marker`
ADD COLUMN `updated_at` TIMESTAMP NOT NULL AFTER `longitude`,
ADD COLUMN `created_at` TIMESTAMP NOT NULL AFTER `updated_at`,
ADD COLUMN `deleted_at` VARCHAR(45) NULL AFTER `created_at`;
UPDATE map_marker set created_at=now() where created_at = '0000-00-00 00:00:00'
     */
    return sequelize.query('UPDATE map_marker set updated_at=now() where updated_at is null;', {type: sequelize.QueryTypes.UPDATE})

    ALTER TABLE tags ADD code varchar(50) NOT NULL ;
}

module.exports.down = function() {
    return;
}