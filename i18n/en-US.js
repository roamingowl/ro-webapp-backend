module.exports = {
    'Search': 'Search',
    'Map': 'Map',
    'settings': 'settings',
    'Plants': 'Plants',
    'Blooming': 'Blooming',
    'Bearing fruits': 'Bearing fruits',
    'Expositions': 'Exhibitions',
    'All plants': 'All plants'
}